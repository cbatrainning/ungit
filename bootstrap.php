<?php
define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/application/'));

// サーバネームの設定ファイルが存在すれば読み込む
if(file_exists("library/config/{$_SERVER['SERVER_NAME']}.php")){
	require_once( "library/config/{$_SERVER['SERVER_NAME']}.php" );
}else{
	require_once( "library/config/default.php" );
}

require_once( CB_FW_ROOT . '/library/CB/bootstrap.php' );