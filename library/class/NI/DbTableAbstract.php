<?php

/**
* DbTableAbstract.php
* 
* @category CB_Framework
* @package CB_Framework
* @copyright CYBRiDGE
* @license CYBRiDGE License 1.0
* @since PHP 5.0
* @link http://www.cybridge.jp/
*/

class NI_DbTableAbstract extends CB_DbTableAbstract{
    
    // pager関数が名称変更になったのでラッパー関数で対応
    function pager($select, $options = null){
    	return $this->fetchPager($select , $options);
    }
    
    // getOptionのラッパー
    function getOptionBySET($field, $keyname = true){
    	$this->getOptions($field, $keyname);
    }
}
