<?php

class CB_SecureForm_Rule_is_challenge extends CB_QuickForm_Rule{
	function validate($value, $options){
		$db_challenge = CB_Factory :: getDbObject("challenge");

		if($options[0]){
			return true;
		}
		else{
			if($db_challenge->isChallenge($value)){
				return false; 
			} 
			else {
				return true;
			}
		}		
   }
}
