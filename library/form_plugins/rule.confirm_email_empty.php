<?php

class CB_SecureForm_Rule_confirm_email_empty extends CB_QuickForm_Rule{
	
	function validate($value, $options = null){
		$member_email = $value[0];
		$confirm_member_email = $value[1];

		if (empty($confirm_member_email)) {
			exit;
		}
		
		if(empty($member_email) && empty($confirm_member_email))
			return false;
		else 
			return true;
	}
	
}