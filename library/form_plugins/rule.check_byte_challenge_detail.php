<?php

class CB_SecureForm_Rule_check_byte_challenge_detail extends CB_QuickForm_Rule{
	
	function validate($value, $options = null){
		$string_byte = mb_strlen($value);
		
		if($string_byte > 120)
			return false;
		else 
			return true;
	}
	
}