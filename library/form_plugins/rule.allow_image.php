<?php
/**
 * エラーメッセージを表示させるためのダミールール
 *
 * @category   CB_Framework
 * @package    CB_Framework
 * @subpackage form_plugin
 * @copyright  CYBRiDGE
 * @license    CYBRiDGE License 1.0
 *
 */

class CB_SecureForm_Rule_allow_image extends CB_QuickForm_Rule
{
	function validate($value, $options = null){
		
		if($value["size"] > 0 && !preg_match("/\.(jpg|jpeg|gif|png)$/i", $value["name"])) {
			return false;
		} else {
			return true;
		}
	}
}