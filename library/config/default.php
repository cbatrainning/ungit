<?php
/*=====================================
    PATHの定義
=====================================*/
define('CB_FRAMEWORK_PRE', 'NI');
define('CB_FW_ROOT'      , "/usr/share/php/CBFramework_3.1");
define('HTDOCS_ROOT'     , realpath(dirname(__FILE__).'/../../'));
define('CB_BASE'         , HTDOCS_ROOT);
define('CB_PUBLIC'         , CB_BASE    . '/public/');
define('CB_SMARTY_CONFIG' , CB_PUBLIC .'_configs/default.global.inc');
define('CB_TMP'          , CB_BASE . "/../tmp");
define('CB_DEBUGGING'    , 1);




/*=====================================
    データベース定義
=====================================*/
define('CB_DB_TYPE' , 'pdo_mysql');
define('CB_DB_HOST' , 'localhost');
define('CB_DB_PORT' , '3306');
define('CB_DB_NAME' , '');
define('CB_DB_USER' , '');
define('CB_DB_PASS' , '');

/*=====================================
    その他の定義
=====================================*/
//define('CB_CONST',false);

